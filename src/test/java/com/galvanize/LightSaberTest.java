package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LightSaberTest {

    @Test
    public void testSetChargeSetsCharge(){
        // setup
        LightSaber darkSaber = new LightSaber(1);
        // execution
        darkSaber.setCharge(90.0f);
        // assertions
        assertEquals(90.0f, darkSaber.getCharge());
    }

    @Test
    public void testSetColorSetsColor(){
        // setup
        LightSaber darkSaber = new LightSaber(1);
        // execution
        darkSaber.setColor("Dark");
        // assertions
        assertEquals("Dark", darkSaber.getColor());
    }

    @Test
    public void testGetChargeReturnsCharge(){
        LightSaber darkSaber = new LightSaber(1);
        float charge = darkSaber.getCharge();
        assertEquals(100.0f, charge );
    }

    @Test
    public void testGetJediSerialNumberReturnsJediSerialNumber(){
        LightSaber darkSaber = new LightSaber(1);
        long jediSerialNumber = darkSaber.getJediSerialNumber();
        assertEquals(1, jediSerialNumber );
    }

    @Test
    public void testGetColorReturnsColor(){
        LightSaber darkSaber = new LightSaber(1);
        String color = darkSaber.getColor();
        assertEquals("green", color);
    }

    @Test
    public void testUseReducesCharge(){
        LightSaber ls = new LightSaber(1);
        ls.use(6.0f);
        assertEquals(99.0f, ls.getCharge());
    }

    @Test
    public void testGetRemainingMinutesReturnsCorrectValue(){
        LightSaber ls = new LightSaber(1);
        float remainingMinutes = ls.getRemainingMinutes();
        assertEquals(300.0f, remainingMinutes);
    }

    @Test
    public void testRechargeSetsFullCharge(){
        LightSaber ls = new LightSaber(1);
        ls.use(6.0f);
        ls.recharge();
        assertEquals(100.0f, ls.getCharge());
    }

}
